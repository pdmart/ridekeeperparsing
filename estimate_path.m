%% Housekeeping
clc; close all; clear all;

%% Load estimated events
events = csvread('outputs/westwood_01_event_estimates.csv');

%% Load raster path image
im_path = rgb2gray(imread('figure/sn_map.png'));
num_rows = size(im_path,1);
num_cols = size(im_path,2);
% make sure anything that's not white is weight: 1
for r=1:num_rows
    im_path(r,im_path(r,:) < 255) = 1.0;
    im_path(r,im_path(r,:) == 255) = 0.0;
    fprintf('%d / %d\n', r, num_rows);

end

%% Find path indices
path_coords = []; % r;c
for r=1:num_rows
    fprintf('%d / %d\n', r, num_rows);
    cols = find(im_path(r,:) == 1.0);
    path_coords = [path_coords [r*ones(1,length(cols),1); cols]];
end


%% Calculate pixel distances to paths
im_weights = im_path;
weight_fcn = @(d)( 1/d );

for i=1:size(path_coords,2);  
        fprintf('%d / %d\n', i, size(path_coords,2));
        im_weights = updatePathWeights(im_weights,...
            path_coords(1,i), path_coords(2,i), weight_fcn);
end

% save weight image
%save('cache/weight_image','im_weights');

%% Plot weight image
imagesc(im_weights);
colorbar;