function [pos vel] = DvKalman(z)

dt = 1.0; % about 1 Hz / 1 sec

persistent A H Q R
persistent x P
persistent firstRun

if isempty(firstRun)
    firstRun = 1;
    
    
    A = [1 dt; 0 1];
    H = [1 0; 0 1];
    Q = [22 0; 0 3.5]; % process
    R = [13 0; 0 0.5]; % measurement
    
    x = [0 0]';
    P = 5*eye(2);
end

xp = A*x;
Pp = A*P*A' + Q;

K = Pp*H'*inv(H*Pp*H' + R);

x = xp + K*(z-H*xp);
P = Pp - K*H*Pp;

pos = x(1);
vel = x(2);
















