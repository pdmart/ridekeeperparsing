%% Housekeeping
% clear screen
clc; 
% clear variables
clear all; 
% close figures
close all;

%% Load results
load('cache/speedest_806');
load('cache/beta_806');
b_806 = b;
sp_806 = speed_prediction;

load('cache/speedest_823');
load('cache/beta_823');
b_823 = b;
sp_823 = speed_prediction;

load('cache/speedest_840');
load('cache/beta_840');
b_840 = b;
sp_840 = speed_prediction;

load('cache/speedest_westwood');
load('cache/beta_westwood');
b_west = b;
sp_west = speed_prediction;

% modeled function
fcn = @(b,x)( b(1) + b(2)*log(b(3)*x) );

%% Plot estimated functions
var_array = 1:2:100;
est_806 = fcn(b_806,var_array);
est_823 = fcn(b_823,var_array);
est_840 = fcn(b_840,var_array);
est_west = fcn(b_west,var_array);

cfigure(20,8);
plot(var_array,est_806, 'o-b','LineWidth',2);
hold on;
plot(var_array,est_823, 's-r','LineWidth',2);
plot(var_array,est_840, '^-m','LineWidth',2);
plot(var_array,est_west, 'x-k','LineWidth',2);
xlabel('Variance (m/s^2)^2','FontSize',12);
ylabel('Speed (m/s)','FontSize',12);
grid on;
legend('Run 1','Run 2','Run 3','Run 4','Location','SE');

saveplot('figs/multiple_regression_models');

%% Determine final estimation errors / stats
time_tested = sp_806(end,1)-sp_806(1,1) + ...
    sp_823(end,1)-sp_823(1,1) + ...
    sp_840(end,1)-sp_840(1,1) + ...
    sp_west(end,1)-sp_west(1,1);
time_tested = time_tested/1e3/60*2; % to min

max_speed = max([sp_806(:,2); sp_823(:,2); sp_840(:,2); sp_west(:,2)]);
avg_speed = mean([sp_806(:,2); sp_823(:,2); sp_840(:,2); sp_west(:,2)]);
err_806 = abs( sp_806(:,2)-sp_806(:,3) );
err_823 = abs( sp_823(:,2)-sp_823(:,3) );
err_840 = abs( sp_840(:,2)-sp_840(:,3) );
err_west = abs( sp_west(:,2)-sp_west(:,3) );
avg_err = mean([err_806; err_823; err_840; err_west]);

fprintf('test time: %.1f min\nmax speed: %.1f m/s\navg speed: %.1f m/s\navg err: %.1f m/s\n',...
    time_tested, max_speed, avg_speed, avg_err);

%% PDF of all speeds
cfigure(20,12);
speeds = [sp_806(:,2); sp_823(:,2); sp_840(:,2); sp_west(:,2)];
moving_speeds = speeds(speeds > 1);
[c,n] = calculatePdf(moving_speeds, 50);
plot(c,n,'s-b','LineWidth',2);
xlabel('Speed (m/s)','FontSize',12);
ylabel('Probability','FontSize',12);
legend('Probability of speeds > 1 m/s');
grid on;
%saveplot('figs/speed_PDF');












