%% Housekeeping
% clear screen
clc; 
% clear variables
clear all; 
% close figures
close all;

%% Load data
filename = 'data/RideKeeperLog_840';
[acc, grav, gyro, acc_lin, mag, euler, gps,...
    displ, speed, heading] = parseRawData(filename);

%% Find yaw rotations (convert raw gyro into rotation about earth normal)
yaw_rotation = zeros(1,min([size(grav,1) size(gyro,1)]));
for i=1:size(


%% Find forward-pointing sensor axis
% start with empty variance array
median_acc = [];
% window (number of 50 Hz samples)
win = 50*0.1; % 2 sec
% calculate accelerometer median


%plot(acc_lin(:,1),medfilt1(acc_lin(:,2),5*50),'b');
%hold on;
%plot(acc_lin(:,1),medfilt1(acc_lin(:,3),5*50),'r')
%plot(acc_lin(:,1),medfilt1(acc_lin(:,4),5*50),'k')

%% Calculate windowed accelerometer variance
% start with empty variance array
variance = [];
% window (number of 50 Hz samples)
win = 2*50;
for i=1:win:(size(acc_lin,1)-win)
    % median = [time; x; y; z]
    variance = [variance [
        acc(i,1)/1e9;
        var((acc_lin(i:i+win,2)));
        var((acc_lin(i:i+win,3)));
        var((acc_lin(i:i+win,4)))
        ]];        
end

plot(variance(1,:)-variance(1,1), sum(variance(2:4,:),1),'r');
hold on;
plot(speed(:,1)/1e3-speed(1,1)/1e3, speed(:,2),'k');


%%

% filter variance
variance_filt = variance;
variance_filt(:,2) = medfilt1(variance(:,2),10);

% force variance and speed to be same size
y = speed;
x = zeros(size(speed,1),size(speed,2));
y(:,1) = y(:,1)./1e3;

for i=1:size(speed,1)
    % find closest variance_filt
    closest_idx = find(variance_filt(:,1) < y(i,1),1,'last');
    if isempty(closest_idx)
        closest_idx = find(variance_filt(:,1) > y(i,1),1,'first');
    end
    x(i,1) = y(i,1); % times more or less the same
    x(i,2) = variance_filt(closest_idx,2);
    
end

cfigure(20,12);
scatter(x(:,2), y(:,2));
hold on;
xlabel('Filtered, windowed accel variance','FontSize',12);
ylabel('Speed from GPS','FontSize',12);
grid on;
%saveplot('figs/variance_to_speed_relation');

% nonlinear regression
fcn = @(b,x)( b(1) + b(2)*log(b(3)*x) );
%fcn = @(b,x)( b(1) + b(2)*x );
b0 = [1 1 1];
%b = nlinfit(x(:,2), y(:,2), fcn, b0);
middle_var = mean(x(:,2));
weights = 20*exp(100*abs((x(:,2)-middle_var)));
[b,R,J,CovB] = nlinfit(x(:,2),y(:,2),fcn,b0,'Weights',weights);
%[b,R,J,CovB] = nlinfit(x(:,2),y(:,2),fcn,b0);
%b = [5 15 20];
delta = (max(x(:,2)) - min(x(:,2)))/100;
fit = fcn(b,[min(x(:,2)):delta:max(x(:,2))]);
plot([min(x(:,2)):delta:max(x(:,2))], fit, 'm');
ylim([0 35]);

prediction = fcn(b, x(:,2));
prediction(prediction < 0) = 0;

cfigure(30,15);
subplot(2,1,1);
mps_to_mph = 2.237;
plot(y(:,1)/(60) - y(1,1)/60,mps_to_mph*y(:,2),'r');
hold on;
plot(x(:,1)/(60)- x(1,1)/60, mps_to_mph*prediction, 'b');
xlabel('Time (min)','FontSize',12);
ylabel('Speed (mph)','FontSize',12);
grid on;
legend('Speed from GPS','Estimated Speed from Variance');

subplot(2,1,2);
plot(y(:,1)/(60) - y(1,1)/60,mps_to_mph*(y(:,2)-prediction),'m');
xlabel('Time (min)','FontSize',12);
ylabel('Prediction Error (mph)','FontSize',12);
grid on;

%saveplot('figs/variance_to_speed_nlin');

% save for kalman
s = prediction;

%% How well can we predict changes in speed using this method?
real = mps_to_mph*y(:,2);
est = mps_to_mph*prediction;

% filter
% butterworth HPF to remove gravity
fc = 0.005;
SR = 1;
fNorm =  fc / (SR / 2);
[b,a] = butter(2, fNorm, 'high');
est_hp = filtfilt(b,a,est);
real_hp = filtfilt(b,a,real);

% butterworth LPF to smooth
fc = 0.1;
SR = 1;
fNorm =  fc / (SR / 2);
[b,a] = butter(2, fNorm, 'low');
est_bp = filtfilt(b,a,est_hp);
real_bp = filtfilt(b,a,real_hp);

cfigure(30,15);
subplot(2,1,1);
mps_to_mph = 2.237;
plot(y(:,1)/(60) - y(1,1)/60,real_bp,'r');
hold on;
plot(x(:,1)/(60)- x(1,1)/60, est_bp, 'b');
xlabel('Time (min)','FontSize',12);
ylabel('Speed Change (mph)','FontSize',12);
grid on;
legend('Speed from GPS','Estimated Speed from Variance');

subplot(2,1,2);
plot(y(:,1)/(60) - y(1,1)/60,real_bp-est_bp,'m');
xlabel('Time (min)','FontSize',12);
ylabel('Prediction Error (mph)','FontSize',12);
grid on;

%saveplot('figs/variance_to_speed_nlin_delta');

%% Predict Acceleration from Accel
est = [(acc_lin(:,1)/1e9+4) acc_lin(:,3)+acc_lin(:,4)]; % y axis

% butterworth LPF to remove noise
fc = 1;
SR = mean(1./(diff(est(:,1))));
fNorm =  fc / (SR / 2);
[b,a] = butter(1, fNorm, 'low');
est_low = [est(:,1) filtfilt(b,a,est(:,2))];

% butterworth HPF to remove gravity
fc = 0.02;
SR = mean(1./(diff(est(:,1))));
fNorm =  fc / (SR / 2);
[b,a] = butter(2, fNorm, 'high');
est_bp = [est_low(:,1) filtfilt(b,a,est_low(:,2))];

% convert speed to acceleration
y = speed;
y(:,1) = y(:,1)./1e3; % convert time to sec
y_acc = diff(y(:,2))./diff(y(:,1)); % convert to m/s^2
SR = 1.0;
y = [y(1:(end-1),1)-(SR/2) y_acc];

% force est to be same size as acceleration vector
x = zeros(size(y,1),size(y,2));
for i=1:size(y,1)
    % find closest variance_filt
    closest_idx = find(est_bp(:,1) < y(i,1),1,'last');
    x(i,1) = y(i,1); % times more or less the same
    x(i,2) = est_bp(closest_idx,2);
    
end


% butterworth LPF to remove noise of true accel
fc = 0.45;
SR = mean(1./(diff(y(:,1))));
fNorm =  fc / (SR / 2);
[b,a] = butter(1, fNorm, 'low');
y_lp = [y(:,1) filtfilt(b,a,y(:,2))];

close all;
cfigure(30,12);
start = x(1,1);
plot( (x(:,1)+0)/60-start/60, 1.8*x(:,2),'r')
hold on;
plot(y_lp(:,1)/60-start/60, y_lp(:,2),'b');
xlabel('Time (min)','FontSize',12);
ylabel('Change in Velocity (mps)','FontSize',12);
grid on;
legend('Predicted change Acc','True Acc from GPS');
%saveplot('figs/predicting_acc_from_acc');

% save for kalman
sdot = 1.8*x(:,2);

%% Plot speed and change in speed
cfigure(30,12);
%plot(speed(:,1), [0; diff(speed(:,2))]);
%hold on;
%plot(speed(:,1), sdot,'r');
scatter(y_lp(:,2), 1.8*x(:,2),'b');


%% Fuse the accel variance and integration techniques in a kalman filter
% what should Q and R be?
fprintf('determining R, the measurement noise\n');
fprintf('   var of s: %f\n', var(s));
fprintf('   var of sdot: %f\n', var(sdot));
fprintf('determining Q, the system noise\n');
fprintf('   var of z: %f\n', var(speed(:,2)));
fprintf('   var of zdot: %f\n', var(y(:,2)));

N = (length(s)-1);
kalman_est = zeros(length(sdot),2);

for i=1:N
    z = [s(i); sdot(i)];
    kalman_est(i,:) = DvKalman(z);
end

% plot
real = speed;
est = kalman_est(:,1);
est(est<0) = 0;
cfigure(35,24);

subplot(2,1,1);
plot(real(1:N,1), real(1:N,2),'b');
hold on;
plot(real(1:N,1), est(1:N), 'r');
plot(real(1:N,1), s(1:N),'k');
legend('true speed','kalman speed', 'variance speed');

subplot(2,1,2);
hold on;
plot(real(1:N,1), abs(real(1:N,2) - est(1:N)), 'r');
plot(real(1:N,1), abs(real(1:N,2) - s(1:N)),'k');
legend('kalman error', 'variance error');






