function [ im_new ] = updatePathWeights( image, row, col, weight_fcn )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
radius = 30; % px

im_new = image;

if image(row,col) == 0
    return;
end


for r=(row-radius):(row+radius)
    for c=(col-radius):(col+radius)
        % ignore if out of bounds
        if r <= 0 || c <= 0 || r > size(image,1) || c > size(image,2)
            continue;
        end
        
        % ignore this exact point
        if r == row && c == col
            continue;
        end
        
        % ignore if not in circle
        dist = sqrt( (r-row)^2 + (c-col)^2 );
        if dist > radius
            continue;
        end
        
        weight = weight_fcn(dist);
        if im_new(r,c) < weight
            im_new(r,c) = weight;
        end
        
    end
end


end

