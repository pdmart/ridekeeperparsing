%% Housekeeping
clc; clear all; close all;

%% Load data
filename = 'data/RideKeeper_13_10_22_drivingTest_ucla_shermanoaks';
[acc, grav, gyro, acc_lin, mag, euler,...
    disp, speed, heading] = parseRawData(filename);


%% Predict Acceleration from Bumps
est = [acc_lin(:,1)/1e9 - acc_lin(1,1)/1e9 acc_lin(:,4).^2]; % z axis

% butterworth LPF to remove really high freq. noise
fc = 0.5;
SR = mean(1./(diff(est(:,1))));
fNorm =  fc / (SR / 2);
[b,a] = butter(2, fNorm, 'low');
est_low = [est(:,1) filtfilt(b,a,est(:,2))];

% butterworth HPF to remove gravity and anything but quick bumps
fc = 0.01;
SR = mean(1./(diff(est(:,1))));
fNorm =  fc / (SR / 2);
[b,a] = butter(2, fNorm, 'high');
est_bp = [est_low(:,1) filtfilt(b,a,est_low(:,2))];

% peak detection

plot(est_bp(:,1), est_bp(:,2))
%spectrogram(est_bp(:,2), 512, 32, 10000, 50);