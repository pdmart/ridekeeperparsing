%% Housekeeping
clc; clear all; close all;

%% Load data
filename = 'data/RideKeeperLog_823';
[acc, grav, gyro, acc_lin, mag, euler,...
    gps, displ, speed, heading] = parseRawData(filename);

%% GPS Time Shift
% for some reason, data sets collected for RideKeeperLog_806/823/840 have
% time shifts between the GPS timestamps and the other sensors, to the tune
% of around 40 seconds! we have to shift it back. In fact, discussions with
% Chenguang indicate this happens from time to time--the two can just get
% out of sync.  In a real system, you would just use most recent data from
% each so this is only a post-processing issue.

GPS_TIME_SHIFT = +40;
gps(:,1) = gps(:,1)+GPS_TIME_SHIFT*1e3;
speed(:,1) = speed(:,1)+GPS_TIME_SHIFT*1e3;
displ(:,1) = displ(:,1)+GPS_TIME_SHIFT*1e3;
heading(:,1) = heading(:,1)+GPS_TIME_SHIFT*1e3;

%% smooth gyro for orientation
% --- Generic filter block, in this case for gyro. to use for any other
% sensor just replace "gyro" and make sure the time division (here: 1e9) is
% the correct one for that sensor type. E.g. GPS = 1e3.

% butterworth LPF
fc = 0.5; % Hz
SR = mean(1./(diff(gyro(:,1)/1e9))); % sampling rate (Hz)
fNorm =  fc / (SR / 2);
[b,a] = butter(1, fNorm, 'low');
gyro_filt(:,1) = gyro(:,1);
% filter rotation about the z (yaw): index 4
% gyro indices:
%     1: about x (pitch), 2: about y (roll), 3: about z (yaw)
gyro_filt(:,2) = filtfilt(b,a,gyro(:,4));


%% Calculate windowed accelerometer variance
% start with empty variance array
variance = [];
% window (seconds * number of 50 Hz samples)
win = 3*50; % 3 second window, 50 samp/sec

for i=1:win:(min([size(acc_lin,1) size(grav,1)]) -win)
    % calculate windowed variance of accel on x,y,z axes
    var_all = [var(sum(acc_lin(i:i+win,2))) var((acc_lin(i:i+win,3))) var((acc_lin(i:i+win,4)))];
    % project to just the vertical axis (normal axis)
    var_normal = grav(i,2:4)*var_all';
    % append to variance matrix (and convert time to seconds);
    variance = [variance;
        acc(i,1)/1e9 var_normal
        ];
end

% ensure minimum variance value is around 0
variance(:,2) = variance(:,2) - min(medfilt1(variance(:,2),100));

% low pass filter on variance (zero phase butterworth)
fc = 0.1;
SR = win/50; % window size / 50 Hz
fNorm =  fc / (SR / 2);
[b,a] = butter(2, fNorm, 'low');
variance_filt = filtfilt(b,a,variance);

%% Perform nonlinear regression on speed vs. variance to determine the
% relationship mapping variance to speed.

% force variance and speed to be same size (x = input, y = output)
y = speed; % ground truth
x = zeros(size(speed,1),size(speed,2)); % variance array
y(:,1) = y(:,1)./1e3; % convert speed to seconds

for i=1:size(speed,1)
    % find closest variance_filt index
    closest_idx = find(variance(:,1) < y(i,1),1,'last');
    if isempty(closest_idx)
        closest_idx = find(variance(:,1) > y(i,1),1,'first');
    end
    x(i,1) = y(i,1); % times more or less the same
    x(i,2) = variance(closest_idx,2);
    
end


% nonlinear regression on GPS speed vs windowed accel. variance
% the fitting function (b is the parameter array)
fcn = @(b,x)( b(1) + b(2)*log(b(3)*x) );
% initial parameter guess (somewhat arbitrary)
b0 = [1 1 1];
% perform the nonlinear fit, b is the fit parameters
[b,R,J,CovB] = nlinfit(x(:,2),y(:,2),fcn,b0);
fprintf('--------------------------------------\n');
fprintf(' The parameters are b = \n');
fprintf('     %.2f, %.2f, %.2f \n', b(1), b(2), b(3));
fprintf('--------------------------------------\n');


% apply function to input (variance)
prediction = fcn(b, x(:,2));
% remove negative speeds
prediction(prediction < 0) = 0;

speed_est = [x(:,1) prediction ]; % time (sec?), speed estimate

%% Detect turns
% windowed, bounded integration
TURN_THRESH = 10; % degrees
turns = [gyro_filt(:,1) zeros(size(gyro_filt,1),1)];
win_size = 50*5; % 5 sec (integral window time)
backoff = 50*4; % 4 sec (temporal backoff)
turn_events = [];

last_speed_idx = 1;
last_speed_est_idx = 1;
last_time = turns(1,1);

% windowed integration (summation)
for i=1:size(gyro,1) %time
    w_idx = i-win_size; % previous history (now - window)
    if w_idx <= 0
        w_idx = 1;
    end
    % integrate
    turns(i,2) = sum(gyro_filt(w_idx:i,2));
end

% -------- turn detection ----------
i = 1;
while i < size(turns,1)
    % skip over everything under the threshold
    while abs(turns(i,2)) < TURN_THRESH && i < size(turns,1)
        if i >= size(turns,1)
            break;
        end
        i = i+1;
    end
    
    % once the threshold is passed, find the next peak
    while abs(turns(i,2)) > abs(turns(i-1,2)) && i < size(turns,1)
        if i >= size(turns,1)
            break;
        end
        i = i+1;
    end
    
    % calculate time since last event
    dT = turns(i,1)-last_time;
    last_time = turns(i,1);
    
    % calculate distance traveled since last event
    speed_idx =     find(speed(:,1)./1e3 <= turns(i,1)./1e9, 1, 'last');
    speed_est_idx = find(speed_est(:,1) <= turns(i,1)./1e9, 1, 'last');
    distance =      1e-9*dT*mean(speed(last_speed_idx:speed_idx,2));
    distance_est =  1e-9*dT*mean(speed_est(last_speed_est_idx:speed_est_idx,2));
    
    %fprintf('dist: %.2f, dist_est: %.2f\n', ...
    %    distance,...
    %    distance_est);
    fprintf('err: %.2f\n', (distance-distance_est));
    
    last_speed_idx = speed_idx;
    last_speed_est_idx = speed_est_idx;
    
    % append to event array
    turn_events = [turn_events;
        turns(i,1) turns(i,2) distance_est distance
        ];
    
    % backoff
    for j=1:backoff
        if i >= size(turns,1)
            break;
        end
        i = i+1;
    end
    
    % and ride it back down
    while abs(turns(i,2)) < abs(turns(i-1,2)) && i < size(turns,1)
        if i >= size(turns,1)
            break;
        end
        i = i+1;
    end
end

% predict turn angles
turn_angles = turn_events;
turn_angles(:,2) = turn_angles(:,2)*1.3;

% plot
cfigure(30,12);
plot(turns(:,1)/1e9 - turns(1,1)/1e9, turns(:,2), 'b','LineWidth',2);
hold on;
plot(turn_events(:,1)/1e9 - turns(1,1)/1e9, turn_events(:,2),'or','LineWidth',2);
xlabel('Time (sec)','FontSize',12);
ylabel('Turn angle (degrees)','FontSize',12);
grid on;
legend('Running Estimate','Detected Turns');
%saveplot('figs/turn_estimate');

% save output file
%csvwrite('outputs/events_01',turn_angles(:,2:end));

%% Make movie
close all;

t_start = acc(1,1)/1e9;
t_stop = acc(end,1)/1e9;
t_window = 60;
t_delta = 0.5;

% Full displacement plot
fig = cfigure(40,20);
subplot(4,2,[1 3 5 7]);
plot(displ(:,2),displ(:,3),'Color',[0.6 0.6 0.6],'LineWidth',3);
hold on;
h_displ = plot(0,0,'or','MarkerSize',10,'LineWidth',2);
xlabel('Longitudinal Displacement (m)','FontSize',12);
ylabel('Latitudinal Displacement (m)','FontSize',12);
legend('Full Path','Current Location','Location','NW');
grid on;

% Speed plot
subplot(4,2,2);
h_speed = plot(0,0,'b','LineWidth',2);
hold on;
h_speedest = plot(0,0,'m','LineWidth',2);
xlabel('Time (s)','FontSize',12);
ylabel('Speed (mph)','FontSize',12);
grid on;
ylim([0 60]);
legend('GPS Speed','Estimated Speed','Location','NW');
est_tracking_offset = 4; % sec

% gyro plot
subplot(4,2,4);
h_gyro = plot(0,0,'r','LineWidth',2);
xlabel('Time (s)','FontSize',12);
ylabel('Filtered Yaw Rate','FontSize',12);
grid on;
ylim([-90 90]);

% turn plot
subplot(4,2,[6 8]);
h_turn = stem(0,0,'k','LineWidth',5);
xlabel('Time (s)','FontSize',12);
ylabel('Estimated Turn (degrees)','FontSize',12);
grid on;
ylim([-130 130]);

% movie object
%{
vidObj = VideoWriter('outputs/Movie2.avi');
vidObj.FrameRate=23;
open(vidObj);
%}

% loop through movie
for t=t_start:t_delta:t_stop
    % update displacement overlay
    last_displ = displ(find(displ(:,1)/1e3 < t, 1, 'last'), :);
    if ~isempty(last_displ)
        set(h_displ,'XData',last_displ(2), 'YData', last_displ(3));
    end
    
    
    % update speed plot
    last_speed = find(speed(:,1)/1e3 < t, 1, 'last');
    win_speed = find(speed(:,1)/1e3 < (t-t_window), 1, 'last');
    if isempty(win_speed)
        win_speed = 1;
    end
    if ~isempty(last_speed)
        x = ( speed(win_speed:last_speed,1) - speed(1,1) )/1e3;
        y = speed(win_speed:last_speed,2);
        set(h_speed,'XData',x,'YData',y);
        y = speed_est(win_speed:last_speed,2);
        set(h_speedest,'XData',x+est_tracking_offset,'YData',y);
        lim_hi = t - t_start;
        lim_lo = lim_hi - t_window;
        subplot(4,2,2);
        xlim([lim_lo lim_hi]);
    end
    
    % update orientation plot
    last_gyro = find(gyro_filt(:,1)/1e9 < t, 1, 'last');
    win_gyro = find(gyro_filt(:,1)/1e9 < (t-t_window), 1, 'last');
    if isempty(win_gyro)
        win_gyro = 1;
    end
    if ~isempty(last_gyro)
        x = ( gyro_filt(win_gyro:last_gyro,1)/1e9 - t_start );
        y = gyro_filt(win_gyro:last_gyro,2);
        set(h_gyro,'XData',x,'YData',100*y);
        lim_hi = t - t_start;
        lim_lo = lim_hi - t_window;
        subplot(4,2,4);
        xlim([lim_lo lim_hi]);
    end
    
    % update turn decision plot
    last_turn = find(turn_angles(:,1)/1e9 < t, 1, 'last');
    win_turn = find(turn_angles(:,1)/1e9 < (t-t_window), 1, 'last');
    if isempty(win_turn)
        win_turn = 1;
    end
    if ~isempty(win_turn)
        x = ( turn_angles(win_turn:last_turn,1)/1e9 - t_start );
        y = turn_angles(win_turn:last_turn,2);
        set(h_turn,'XData',x,'YData',y);
        lim_hi = t - t_start;
        lim_lo = lim_hi - t_window;
        subplot(4,2,[6 8]);
        xlim([lim_lo lim_hi]);
    end
    
    % append to movie
    %{
    f = getframe(fig);
    writeVideo(vidObj,f);
    %}
    
    % prevent race condition
    pause(0.01);
    
end

close(vidObj);

%% Plot estimated output

x = [0];
y = [0];
last_angle = 90; % due south

for i=1:size(turn_angles,1);
    segment_dist = turn_angles(i,3);
    segment_angle = turn_angles(i,2);
    new_x = x(end) + cosd(last_angle)*segment_dist;
    new_y = y(end) + sind(last_angle)*segment_dist;
    
    last_angle = last_angle + segment_angle;
    
    x = [x new_x];
    y = [y new_y];
    
    
end

plot(x,y,'b','LineWidth',2);
hold on;
plot(displ(:,2), displ(:,3),'r','LineWidth',2);
xlabel('Longitudinal Displacement (m)','FontSize',12);
ylabel('Latitudinal Displacement (m)','FontSize',12);
grid on;
legend('Estimate','True','Location','NW');
%saveplot('figs/path_estimate_806');



%% save fake output file


% error percentage is such that 2sigma (97.7%) of errors are <= x %
for e=[0 10 20 30 40 50 60 70 80 90 100]
    distances = turn_angles(:,4); % ideal
    x = (e/100).*distances;
    sigma = x./2;
    distances = distances + sigma.*randn(size(distances,1),1);
    distances(distances < 0) = 0;
    
    f_title = sprintf('events_02_error_%.2d\n', e);
    %csvwrite(['outputs/' f_title],[(turn_angles(:,1)/1e9 - turn_angles(1,1)/1e9) distances turn_angles(:,2)]);
end

x = [0];
y = [0];
last_angle = 270; % due south

for i=1:size(turn_angles,1);
    segment_dist = distances(i);
    segment_angle = turn_angles(i,2);
    new_x = x(end) + cosd(last_angle)*segment_dist;
    new_y = y(end) + sind(last_angle)*segment_dist;
    
    last_angle = last_angle + segment_angle-2;
    
    x = [x new_x];
    y = [y new_y];
    
    
end

plot(x,y,'b','LineWidth',2);
hold on;
plot(displ(:,2), displ(:,3),'r','LineWidth',2);
xlabel('Longitudinal Displacement (m)','FontSize',12);
ylabel('Latitudinal Displacement (m)','FontSize',12);
grid on;
legend('Estimate','True');
%saveplot('figs/path_estimate_02');

%% Save displacement to file
d = displ;
d(:,1) = d(:,1)/1e3 - d(1,1)/1e3;
%dlmwrite('outputs/displacement_02', d,'precision',7);

g = gps;
g(:,1) = g(:,1)/1e3 - g(1,1)/1e3;
%dlmwrite('outputs/gps_02',g,'precision',7);

































