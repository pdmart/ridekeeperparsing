%% Housekeeping
clc; close all; clear all;

%% definitions
TYPE_LINEAR_ACCELERATION = 10;
TYPE_GYROSCOPE = 4;
TYPE_GPS = 100;
MPS_TO_MPH = 2.23694;

%% circular buffers
BUFF_SIZE = 3*50; % x sec * 50
accel_buff = zeros(3,BUFF_SIZE);
gyro_buff = zeros(3,500);
gps_buff = zeros(3,BUFF_SIZE);

%% state variables
accel_var = 0;
speed = 0;
SAVE_SIZE = 500; % window size of variables for plotting
speed_array = nan(1,SAVE_SIZE);
speed_array_real = nan(1,20);
gyro_filt = zeros(1,size(gyro_buff,2));

%% set up speed function
speed_fcn = @(b,x)( b(1) + b(2)*log(b(3)*x) );
beta = [1.37 3.46 1.63]; % from RideKeeperLog_806

%% set up gyro filtering
fc = 0.5;
SR = 50;
fNorm =  fc / (SR / 2);
[b,a] = butter(1, fNorm, 'low');

%% gyro turn matching
turns = zeros(1,size(gyro_buff,2));
win_size = 50*1; % 5 sec


%% set up live plot
cfigure(50,40);
% Speed est
subplot(3,1,1);
h_speed = plot(0,0,'s-b','LineWidth',1);
hold on;
xlabel('Time (s)','FontSize',16);
ylabel('Estimated Speed (mph)','FontSize',16);
grid on;
ylim([0 70]);

% Speed real
subplot(3,1,2);
h_speed_real = plot(0,0,'^-m','LineWidth',1);
xlabel('Time (s)','FontSize',16);
ylabel('GPS Speed (mph)','FontSize',16);
grid on;
ylim([0 70]);

% gyro plot
subplot(3,1,3);
h_gyro = plot(0,0,'o-r','LineWidth',1);
xlabel('Time (s)','FontSize',16);
ylabel('Predicted Turn','FontSize',16);
grid on;
ylim([-100 100]);

pause(0.1);


%% Set up UDP connection
outport = 4000+randi(20000);
localudp = udp('127.0.0.1', outport,'LocalPort', 8895);
fopen(localudp);

socket_open = true;

REFRESH_PERIOD = 25;
refresh_counter = 0;

while socket_open
    packet = fscanf(localudp);
    
    % packet should be CSV in the format:
    % type, val1, val2, val3
    line = strsplit(packet,',');
    if length(line) < 4 % type + 3 vals
        continue;
    end
    
    type = str2double(line(1));
    vals = str2double(line(2:4))';
    
    
    switch type
        case TYPE_LINEAR_ACCELERATION
            accel_buff = [accel_buff(:,2:end) vals];
        case TYPE_GYROSCOPE
            gyro_buff = [gyro_buff(:,2:end) vals];
            turns = [turns(2:end) 1.3*sum(gyro_filt((end-win_size):end))];
        case TYPE_GPS
            gps_buff = [gps_buff(:,2:end) vals];
            speed_array_real = [speed_array_real(:,2:end) vals(1)];
        otherwise
            warning('unhandled sensor type');
    end
    % update accel variance
    accel_var = var(sum(accel_buff));
    
    % update speed prediction
    speed = speed_fcn(beta,accel_var);
    speed = max(speed,0);
    speed_array = [speed_array(2:end) MPS_TO_MPH*speed];
    
    refresh_counter = refresh_counter + 1;
    
    if refresh_counter == REFRESH_PERIOD
        refresh_counter = 0;
        
        % update est speed
        set(h_speed,'XData',length(speed_array):-1:1,'YData',speed_array);
        
        % update true speed
        set(h_speed_real,'XData',length(speed_array_real):-1:1,'YData', MPS_TO_MPH*speed_array_real);
        
        % filter gyro
        gyro_filt = filtfilt(b,a,gyro_buff(3,:));
        set(h_gyro,'XData',length(gyro_filt):-1:1,'YData',turns);
        
        drawnow;
    end
end


%% close tcp connection
fclose(localudp);