'''
Created on Nov 11, 2013

@author: suneil
'''

import os
from PIL import Image
    
output = Image.new("RGBA", (19*255, 15*255), (255, 255, 255, 0))

folders = os.listdir("D:\\Tiles\\17")

for i in range(len(folders)):
    files = os.listdir("D:\\Tiles\\17\\" + folders[i])
    for j in range(len(files)):
        tile = Image.open("D:\\Tiles\\17\\"+folders[i]+"\\"+files[j])
        output.paste(tile, (i*255, j*255))
        
output.save("E:\\map.png", "PNG")