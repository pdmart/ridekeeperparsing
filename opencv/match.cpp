/**
 * @file MatchTemplate_Demo.cpp
 * @brief Sample code to use the function MatchTemplate
 * @author OpenCV team
 */

#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>
#include <stdio.h>

using namespace cv;

/// Global Variables
Mat img; Mat templ; Mat templ_resize; Mat result;
const char* image_window = "Source Image";
const char* result_window = "Result window";

int match_method;
int max_Trackbar = 5;

Mat src1, src_gray1, src2, src_gray2;
Mat dst1, detected_edges1, dst2, detected_edges2;

int edgeThresh = 1;
int lowThreshold;
int const max_lowThreshold = 100;
int ratio = 3;
int kernel_size = 3;
const char* window_name = "Edge Map";

/// Function Headers
void MatchingMethod( int, void* );

/**
 * @function CannyThreshold
 * @brief Trackbar callback - Canny thresholds input with a ratio 1:3
 */
static void CannyThreshold(int, void*)
{

    /// Create a matrix of the same type and size as src (for dst)
    dst1.create( src1.size(), src1.type() );

    /// Convert the image to grayscale
    cvtColor( src1, src_gray1, COLOR_BGR2GRAY );

    /// Reduce noise with a kernel 3x3
    blur( src_gray1, detected_edges1, Size(3,3) );

    /// Canny detector
    Canny( detected_edges1, detected_edges1, lowThreshold, lowThreshold*ratio, kernel_size );

    /// Using Canny's output as a mask, we display our result
    dst1 = Scalar::all(0);

    src1.copyTo( dst1, detected_edges1);
    //imshow( window_name, dst );

        /// Create a matrix of the same type and size as src (for dst)
    dst2.create( src2.size(), src2.type() );

    /// Convert the image to grayscale
    cvtColor( src2, src_gray2, COLOR_BGR2GRAY );

    /// Reduce noise with a kernel 3x3
    blur( src_gray2, detected_edges2, Size(3,3) );

    /// Canny detector
    Canny( detected_edges2, detected_edges2, lowThreshold, lowThreshold*ratio, kernel_size );

    /// Using Canny's output as a mask, we display our result
    dst2 = Scalar::all(0);

    src2.copyTo( dst2, detected_edges2);

    std::cout << detected_edges2.size() << std::endl;
    std::cout << detected_edges2 << std::endl;
}

/**
 * @function main
 */
int main( int, char** argv )
{
  /// Load an image
  src1 = imread( argv[1] );
  src2 = imread( argv[2] );

  // // /// Create a window
  // namedWindow( window_name, WINDOW_AUTOSIZE );

  // // /// Create a Trackbar for user to enter threshold
  // createTrackbar( "Min Threshold:", window_name, &lowThreshold, max_lowThreshold, CannyThreshold );

  /// Show the image
  CannyThreshold(0, 0);

  /// Load image and template
  //img = imread( argv[1], 1 );
  img = dst1;
  templ = dst2;

  /// Create windows
  namedWindow( image_window, WINDOW_AUTOSIZE );
  namedWindow( result_window, WINDOW_AUTOSIZE );

  /// Create Trackbar
  const char* trackbar_label = "Method: \n 0: SQDIFF \n 1: SQDIFF NORMED \n 2: TM CCORR \n 3: TM CCORR NORMED \n 4: TM COEFF \n 5: TM COEFF NORMED";
  createTrackbar( trackbar_label, image_window, &match_method, max_Trackbar, MatchingMethod );

  MatchingMethod( 0, 0 );

  waitKey(0);
  return 0;
}

/**
 * @function MatchingMethod
 * @brief Trackbar callback
 */
void MatchingMethod( int, void* )
{
  /// Source image to display
  Mat img_display;
  img.copyTo( img_display );

  /// Create the result matrix
  int result_cols =  img.cols - templ.cols + 1;
  int result_rows = img.rows - templ.rows + 1;

  result.create( result_cols, result_rows, CV_32FC1 );

  /// Do the Matching and Normalize
  matchTemplate( img, templ, result, match_method );
  normalize( result, result, 0, 1, NORM_MINMAX, -1, Mat() );

  /// Localizing the best match with minMaxLoc
  double minVal; double maxVal; Point minLoc; Point maxLoc;
  Point matchLoc;

  minMaxLoc( result, &minVal, &maxVal, &minLoc, &maxLoc, Mat() );


  /// For SQDIFF and SQDIFF_NORMED, the best matches are lower values. For all the other methods, the higher the better
  if( match_method  == TM_SQDIFF || match_method == TM_SQDIFF_NORMED )
    { matchLoc = minLoc; }
  else
    { matchLoc = maxLoc; }

  /// Show me what you got
  rectangle( img_display, matchLoc, Point( matchLoc.x + templ.cols , matchLoc.y + templ.rows ), Scalar::all(0), 2, 8, 0 );
  rectangle( result, matchLoc, Point( matchLoc.x + templ.cols , matchLoc.y + templ.rows ), Scalar::all(0), 2, 8, 0 );

  imshow( image_window, img_display );
  imshow( result_window, result );

  return;
}
