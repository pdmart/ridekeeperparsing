%% Housekeeping
% clear screen
clc; 
% clear variables
clear all; 
% close figures
close all;

%% Load data
filename = 'data/westwood_01';
[acc, grav, gyro, acc_lin, mag, euler, gps,...
    displ, speed, heading] = parseRawData(filename);

%% GPS Time Shift
% for some reason, data sets collected for RideKeeperLog_806/823/840 have
% time shifts between the GPS timestamps and the other sensors, to the tune
% of around 40 seconds! we have to shift it back. In fact, discussions with
% Chenguang indicate this happens from time to time--the two can just get
% out of sync.  In a real system, you would just use most recent data from
% each so this is only a post-processing issue.

GPS_TIME_SHIFT = -9;
gps(:,1) = gps(:,1)+GPS_TIME_SHIFT*1e3;
speed(:,1) = speed(:,1)+GPS_TIME_SHIFT*1e3;
displ(:,1) = displ(:,1)+GPS_TIME_SHIFT*1e3;
heading(:,1) = heading(:,1)+GPS_TIME_SHIFT*1e3;

%% smooth heading
heading_filt(:,2) = medfilt1(heading(:,2),5);

%% Find turns
delta_sum = 0;
true_turns = [];

for i=2:size(heading_filt,1)
    delta = heading_filt(i,2) - heading_filt(i-1,2);
    delta_sum = delta_sum + delta;
    
    if abs(delta_sum) > 20
        true_turns = [true_turns;
            heading_filt(i,1) delta_sum gps(i,2) gps(i,3) displ(i,2) displ(i,3)];
        delta_sum = 0;

    end
end

%% Plot
plot(displ(:,2),displ(:,3));
hold on;
plot(true_turns(:,5), true_turns(:,6), 'xr','MarkerSize',10);
dlmwrite('outputs/gps_turn_detection',true_turns,'Precision',7);


