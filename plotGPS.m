%% Plot GPS over time
close all;
figure();
% in short distances, 0.01 is about 1.112 km
conversionToKm = 1.112/0.01;
origin = [-118.4434 34.0683];
% center data and convert
x = (gps(:,2) - origin(1))*conversionToKm*1000;
y = (gps(:,3) - origin(2))*conversionToKm*1000;

plot(x, y, 'o-b', 'MarkerFaceColor','b','MarkerSize',5);
axis equal;
grid on;

% save output for uploading to: http://www.gpsvisualizer.com/
dlmwrite('outputmap.csv', gps(:,2:3), 'delimiter', ',', 'precision', 9); 