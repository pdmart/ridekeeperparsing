%% Housekeeping
% clear screen
clc; 
% clear variables
clear all; 
% close figures
close all;

%% Load data
filename = 'data/RideKeeperLog_823';
[acc, grav, gyro, acc_lin, mag, euler, gps,...
    displ, speed, heading] = parseRawData(filename);

%% GPS Time Shift
% for some reason, data sets collected for RideKeeperLog_806/823/840 have
% time shifts between the GPS timestamps and the other sensors, to the tune
% of around 40 seconds! we have to shift it back. In fact, discussions with
% Chenguang indicate this happens from time to time--the two can just get
% out of sync.  In a real system, you would just use most recent data from
% each so this is only a post-processing issue.

GPS_TIME_SHIFT = +40;
gps(:,1) = gps(:,1)+GPS_TIME_SHIFT*1e3;
speed(:,1) = speed(:,1)+GPS_TIME_SHIFT*1e3;
displ(:,1) = displ(:,1)+GPS_TIME_SHIFT*1e3;
heading(:,1) = heading(:,1)+GPS_TIME_SHIFT*1e3;


%% Find yaw rotations (convert raw gyro into rotation about earth normal)
% note, the full accurate solution requires rotation matrices, but i'm
% ommitting that now. This is a good approximation for now (small angle).
yaw_rotation = zeros(min([size(grav,1) size(gyro,1)]),2);
for i=1:size(yaw_rotation,1)
    % normalization factor (sum of grav. values)
    z = sum(abs(grav(i,2:4)));
    yaw_rotation(i,:) = [grav(i,1) (1/z)*grav(i,2:4)*gyro(i,2:4)'];
end


%% Find forward-pointing sensor axis
% for now, for simplification we will assume that the phone is oriented
% like such:
%
%  front
%  ____     ^
% |    |    | +y
% |    |    |
% |    | +z o----> +x
%  ****

%% Calculate windowed accelerometer variance
% start with empty variance array
variance = [];
% window (number of 50 Hz samples)
win = 3*50;

for i=1:win:(min([size(acc_lin,1) size(grav,1)]) -win)
    % calculate windowed variance of accel on x,y,z axes
    var_all = [var(sum(acc_lin(i:i+win,2))) var((acc_lin(i:i+win,3))) var((acc_lin(i:i+win,4)))];
    % project to just the vertical axis (normal axis)
    var_normal = grav(i,2:4)*var_all';
    % append to variance matrix;
    variance = [variance;
        acc(i,1)/1e9 var_normal
        ];        
end

% ensure minimum variance value is around 0
variance(:,2) = variance(:,2) - min(medfilt1(variance(:,2),100));

% low pass filter on variance (zero phase butterworth)
fc = 0.1;
SR = win/50;
fNorm =  fc / (SR / 2);
[b,a] = butter(2, fNorm, 'low');
variance_filt = filtfilt(b,a,variance);

%plot(variance_filt(:,1),variance_filt(:,2),'m');
hold on;
plot(variance(:,1),variance(:,2),'r');
plot((speed(:,1))/1e3, speed(:,2),'k');


%% Perform nonlinear regression on speed vs. variance to determine the
% relationship mapping variance to speed.

% force variance and speed to be same size (x = input, y = output)
y = speed; % ground truth
x = zeros(size(speed,1),size(speed,2)); % variance array
y(:,1) = y(:,1)./1e3; % convert speed to seconds

for i=1:size(speed,1)
    % find closest variance_filt index
    closest_idx = find(variance(:,1) < y(i,1),1,'last');
    if isempty(closest_idx)
        closest_idx = find(variance(:,1) > y(i,1),1,'first');
    end
    x(i,1) = y(i,1); % times more or less the same
    x(i,2) = variance(closest_idx,2);
    
end

% plot GPS speed vs windowed accel. variance
cfigure(20,8);
scatter(x(:,2), y(:,2),'LineWidth',2);
hold on;
xlabel('Filtered, windowed accel variance','FontSize',12);
ylabel('Speed from GPS','FontSize',12);
grid on;

% nonlinear regression on GPS speed vs windowed accel. variance
% the fitting function (b is the parameter array)
fcn = @(b,x)( b(1) + b(2)*log(b(3)*x) );
%fcn = @(b,x)( b(1) + b(2)*x );
% initial parameter guess (somewhat arbitrary)
b0 = [1 1 1];

% perform the nonlinear fit, b is the fit parameters
[b,R,J,CovB] = nlinfit(x(:,2),y(:,2),fcn,b0);
fprintf('--------------------------------------\n');
fprintf(' The parameters are b = \n');
fprintf('     %.2f, %.2f, %.2f \n', b(1), b(2), b(3));
% grap an overlay of the fit curve (delta is just for 
delta = (max(x(:,2)) - min(x(:,2)))/100;
fit = fcn(b,[min(x(:,2)):delta:max(x(:,2))]);
plot([min(x(:,2)):delta:max(x(:,2))], fit, 'm','LineWidth',2);
ylim([0 35]);
legend('Speed vs. Variance','Logarithmic Fit');
% save the plot
saveplot('figs/variance_to_speed_relation');

% apply function to input (variance)
prediction = fcn(b, x(:,2));
% remove negative speeds
prediction(prediction < 0) = 0;

% plot actual vs. predicted speed
cfigure(40,8);
subplot(1,2,1);
mps_to_mph = 2.237;
plot(y(:,1)/(60) - y(1,1)/60,mps_to_mph*y(:,2),'r','LineWidth',2);
hold on;
plot(x(:,1)/(60)- x(1,1)/60, mps_to_mph*prediction, 'b','LineWidth',2);
xlabel('Time (min)','FontSize',14);
ylabel('Speed (mph)','FontSize',14);
grid on;
legend('Speed from GPS','Estimated Speed from Variance');


subplot(1,2,2);
plot(y(:,1)/(60) - y(1,1)/60,mps_to_mph*(y(:,2)-prediction),'m','LineWidth',2);
xlabel('Time (min)','FontSize',14);
ylabel('Prediction Error (mph)','FontSize',14);
grid on;

saveplot('figs/variance_to_speed_nlin');

% save for kalman
s = prediction;

%% Save cache for error comparison
speed_prediction = [speed s]; % time, true speed, est
%save('cache/speedest_westwood','speed_prediction');
%save('cache/beta_westwood','b');

%% Predict True Acceleration from Accelerometer
acc_est = [acc_lin(:,1)/1e9 acc_lin(:,3)]; % y axis

% butterworth LPF to remove noise from raw accelerometer data
fc = 1;
SR = mean(1./(diff(acc_est(:,1))));
fNorm =  fc / (SR / 2);
[b,a] = butter(1, fNorm, 'low');
est_low = [acc_est(:,1) filtfilt(b,a,acc_est(:,2))];

% butterworth HPF to remove any leftover gravity components from accel.
% (note that linear acceleration should already have this removed, but
% maybe I'm just not satisfied with Android's high pass filter, OK!?)
fc = 0.02;
SR = mean(1./(diff(acc_est(:,1))));
fNorm =  fc / (SR / 2);
[b,a] = butter(2, fNorm, 'high');
est_bp = [est_low(:,1) filtfilt(b,a,est_low(:,2))];

% convert true GPS speed to acceleration
y = speed;
y(:,1) = y(:,1)./1e3; % convert time to sec
y_acc = diff(y(:,2))./diff(y(:,1)); % convert to m/s^2
SR = 1.0;
y = [y(1:(end-1),1)-(SR/2) y_acc];

% force est to be same size as acceleration vector so we can compare the
% two
x = zeros(size(y,1),size(y,2));
for i=1:size(y,1)
    % find closest variance_filt
    closest_idx = find(est_bp(:,1) < y(i,1),1,'last');
    x(i,1) = y(i,1); % times more or less the same
    x(i,2) = est_bp(closest_idx,2);
    
end


% butterworth LPF to remove noise of true GPS accel
fc = 0.3;
SR = mean(1./(diff(y(:,1))));
fNorm =  fc / (SR / 2);
[b,a] = butter(1, fNorm, 'low');
y_lp = [y(:,1) filtfilt(b,a,y(:,2))];

close all;
cfigure(30,12);
start = x(1,1);
plot( (x(:,1)+0)/60-start/60, 1.8*x(:,2),'r')
hold on;
plot(y_lp(:,1)/60-start/60, y_lp(:,2),'b');
xlabel('Time (min)','FontSize',12);
ylabel('Change in Velocity (mps)','FontSize',12);
grid on;
legend('Predicted change Acc','True Acc from GPS');
%saveplot('figs/predicting_acc_from_acc');

% save for kalman
sdot = 1.8*x(:,2);
sdot_time = x(:,1);

%% Predict velocity from accelerometer, using variance to reset velocity
% to zero when we detect the car isn't moving
iscarstopped = variance(:,2) < 2;
start_array = diff(iscarstopped) < 0;
stop_array = diff(iscarstopped) > 0;
start_times = variance(start_array,1);
stop_times = variance(stop_array,1);

inst_speed = 0;
speed_est = zeros(size(sdot,1),2);

for i=1:size(sdot,1)
    time = sdot_time(i);
    % was the last event a stop or a start? if stop, speed is 0
    last_stop = find(stop_times < time, 1,'last');
    last_start = find(start_times < time, 1,'last');
    if last_stop > last_start & time-stop_times(last_stop) > 2
        inst_speed = 0;
    end
    
    % integrate acceleration to get speed
    if i == 1
        dT = 0;
    else
        dT = time-sdot_time(i-1);
    end
    inst_speed = inst_speed + sdot(i)*dT;
    
    % disallow negative speed
    if inst_speed < 0
        inst_speed = 0;
    end
    
    speed_est(i,1) = time;
    speed_est(i,2) = inst_speed;
    
end

cfigure(30,12);
plot(speed_est(:,1)-speed_est(1,1), speed_est(:,2)*2, 'r','LineWidth',2);
hold on;
plot(speed(:,1)/1e3 - speed_est(1,1), speed(:,2), 'k','LineWidth',2);
grid on;
xlabel('Time (sec)','FontSize',12);
ylabel('Speed (mps)','FontSize',12);
legend('Estimated Speed','True Speed');
saveplot('figs/estimated_speed_from_accel_integration');

%% Fuse the accel variance and integration techniques in a kalman filter
% currently it looks like the best thing to do is to just use the accel.
% variance results, but i'm sure there are some fancy things we can do here
% for future work

% what should Q and R be?
fprintf('determining R, the measurement noise\n');
fprintf('   var of s: %f\n', var(s));
fprintf('   var of sdot: %f\n', var(sdot));
fprintf('determining Q, the system noise\n');
fprintf('   var of z: %f\n', var(speed(:,2)));
fprintf('   var of zdot: %f\n', var(y(:,2)));

N = (length(s)-1);
kalman_est = zeros(length(sdot),2);

for i=1:N
    z = [s(i); sdot(i)];
    kalman_est(i,:) = DvKalman(z);
end

% plot
real = speed;
acc_est = kalman_est(:,1);
acc_est(acc_est<0) = 0;
cfigure(35,24);

subplot(2,1,1);
plot(real(1:N,1), real(1:N,2),'b');
hold on;
plot(real(1:N,1), acc_est(1:N), 'r');
plot(real(1:N,1), s(1:N),'k');
legend('true speed','kalman speed', 'variance speed');

subplot(2,1,2);
hold on;
plot(real(1:N,1), abs(real(1:N,2) - acc_est(1:N)), 'r');
plot(real(1:N,1), abs(real(1:N,2) - s(1:N)),'k');
legend('kalman error', 'variance error');






