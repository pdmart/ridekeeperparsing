%% Housekeeping
clc; clear all; close all;

%% Load data
filename = 'data/RideKeeper_13_10_22_drivingTest_ucla_shermanoaks';
[acc, grav, gyro, acc_lin, mag, euler,...
    disp, speed, heading] = parseRawData(filename);

%% Estimate speed from accel variance
variance = [];

win = 50; % 1 sec
for i=1:win:(size(acc,1)-win)
    variance = [variance;
        acc(i,1)/1e9 var(acc(i:i+win,4))
        ];        
end

% filter variance
variance_filt = variance;
variance_filt(:,2) = medfilt1(variance(:,2),10);

% nonlinear fit
fcn = @(b,x)( b(1) + b(2)*log(b(3)*x) );
b0 = [0 0 1];
% b = [16.6560 6.8532 1.0134]; for paul
b = [10.391196416437303  14.703636896958871  13.860315356913896]; % for suneil

% generate predicted speed
speed_est = fcn(b, variance_filt(:,2));
% no speeds less than 0
speed_est(speed_est < 0) = 0;

s = [variance(:,1)-variance(1,1)    speed_est];

%% Estimate orientation from Android S/W sensor
orientation_est = 1.4*(180/pi)*euler(:,2);
orientation_est = medfilt1(orientation_est,50);
o = [(euler(:,1)-euler(1,1))/1e9 orientation_est];

%% Filter these things a wee bit more
s(:,2) = medfilt1(s(:,2),5);
o(:,2) = medfilt1(o(:,2),500);

%% Loop through and generate events
THETA_THRESH = 15; % degrees
DISTANCE_THRESH = 30; % meters

events = [];
distance_traveled = 0;

s_idx = 1;
o_idx = 1;

last_speed = 0;
last_orientation = 0;
sum_distance = 0;
all_distance = 0;
o_flag = 0;
s_flag = 0;

% first event
path_end = 1;
if s_idx + 1 < size(s,1)
    s_idx = s_idx + 1;
    path_end = 0;
    s_flag = 1;
end
if o_idx + 1 < size(o,1)
    o_idx = o_idx + 1;
    path_end = 0;
    o_flag = 1;
end
% (time, distance, delta angle, heading)
events = [events;
    s(s_idx,1)  0   0   o(o_idx,1)];

while ~path_end
    
    % advance last index used
    path_end = 1;
    if s_flag && s_idx + 1 < size(s,1)
        s_idx = s_idx + 1;
        path_end = 0;
    end
    if o_flag && o_idx + 1 < size(o,1)
        o_idx = o_idx + 1;
        path_end = 0;
    end
    
    % find next sample, time-wise
    t_o = o(o_idx,1);
    t_s = s(s_idx,1);
    % reset flags
    o_flag = 0;
    s_flag = 0;
    
    if t_o < t_s
        % orientation sample is next
        o_flag = 1;
        if abs( o(o_idx,2)-last_orientation ) > THETA_THRESH && ...
                sum_distance > DISTANCE_THRESH; 
            % generate a new event
            % (time, distance, delta angle, heading)
            events = [events;
                t_o     sum_distance   o(o_idx,2)-last_orientation    o(o_idx,2)];
            last_orientation = o(o_idx,2);
        end
    else
        % speed sample is next
        s_flag = 1;
        % add to distance since last event
        dT = t_s - s(s_idx-1,1);
        sum_distance = sum_distance + dT*s(s_idx,2);
        distance_traveled = distance_traveled + dT*s(s_idx,2);
    end
    
    
end

%% Plot results
cfigure(40,12);
plot(o(:,1), o(:,2),'b');
hold on;
plot(s(:,1), s(:,2)','r');

stem(events(:,1), ones(size(events,1),1),'k');
grid on;
xlim([750 950]);

legend('Estimated Orientation', 'Estimated Speed', 'Event Markers');

%% Write events to file
csvwrite('outputs/events_ucla_to_shermanoaks.csv', events);



