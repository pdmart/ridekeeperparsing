%% Housekeeping
clc; clear all; close all;

%% Load data
filename = 'data/westwood_01';
[acc, grav, gyro, acc_lin, mag, euler,...
    gps, disp, speed, heading] = parseRawData(filename);

%% Compare orientation
% filter euler angle
est = 1.4*(180/pi)*euler(:,2);
est = medfilt1(est,50);
cfigure(25,12);
start = euler(1,1)/(1e9*60);
plot(euler(:,1)/(1e9*60) - start,est,'b');
hold on;
plot(heading(:,1)/(60*1e3) - start,heading(:,2),'r');
xlabel('Time (min)','FontSize',12);
ylabel('Orientation (degrees)','FontSize',12);
grid on;
legend('Heading Estimate','HPF True Heading');
saveplot('figs/predictOrientation');

%% Compare changes in orientation
% convert heading to change in orientation
delta_heading = [];
for i=2:size(heading,1)
    h1 = heading(i-1,2);
    h2 = heading(i,2);
    deltas = [h2-h1 360+h2-h1 -360+h2-h1];
    [m,j] = min(abs(deltas));
    delta_heading = [delta_heading;
        heading(i,1)/1e3 deltas(j)];
end

% filter heading
% butterworth LPF
fc = 0.01;
SR = mean(1./(diff(heading(:,1)/1e3)));
fNorm =  fc / (SR / 2);
[b,a] = butter(1, fNorm, 'low');
heading_filt(:,1) = delta_heading(:,1);
heading_filt(:,2) = filtfilt(b,a,delta_heading(:,2));

% convert euler to change in orientation
delta_euler = [];
for i=2:size(euler,1)
    h1 = (180/pi)*euler(i-1,2);
    h2 = (180/pi)*euler(i,2);
    deltas = [h2-h1 360+h2-h1 -360+h2-h1];
    [m,j] = min(abs(deltas));
    delta_euler = [delta_euler;
        euler(i,1)/1e9 deltas(j)];
end

% filter euler
% butterworth LPF
fc = 20;
SR = mean(1./(diff(delta_euler(:,1))));
fNorm =  fc / (SR / 2);
[b,a] = butter(1, fNorm, 'low');
euler_filt(:,1) = delta_euler(:,1);
euler_filt(:,2) = filtfilt(b,a,delta_euler(:,2));

plot(heading_filt(:,1) - heading_filt(1,1), heading_filt(:,2));
hold on;
plot(euler_filt(:,1) - euler_filt(1,1), euler_filt(:,2),'r');


%% Predict orientation from accel
est = [acc_lin(:,1)/1e9 acc_lin(:,2)];
% median filter
est(:,2) = medfilt1(acc_lin(:,2), 20);

% butterworth LPF
fc = 0.01;
SR = mean(1./(diff(est(:,1))));
fNorm =  fc / (SR / 2);
[b,a] = butter(1, fNorm, 'low');
est(:,2) = filtfilt(b,a,est(:,2));



close all;
cfigure(30,12);
plot(est(:,1)-est(1,1), 30*est(:,2), 'b');
hold on;
plot(heading_filt(:,1) - heading_filt(1,1), heading_filt(:,2),'r');
plot([0 900],[0 0],'--k');

%% Predict orientation from gyro
est = [gyro(:,1)/1e9 gyro(:,4)];
% median filter
est(:,2) = medfilt1(gyro(:,2), 20);

% butterworth LPF
fc = 0.1;
SR = mean(1./(diff(est(:,1))));
fNorm =  fc / (SR / 2);
[b,a] = butter(1, fNorm, 'low');
est(:,2) = filtfilt(b,a,est(:,2));



close all;
cfigure(30,12);
plot(est(:,1)-est(1,1), 2000*est(:,2), 'b');
hold on;
plot(heading_filt(:,1) - heading_filt(1,1), heading_filt(:,2),'r');
plot([0 900],[0 0],'--k');














