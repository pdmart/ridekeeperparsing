%% Try getting the speed and distance travelled
% format: timestamp, speed, distance
n = size(accel_lin, 1);
xAxis = zeros(n, 3);
xAxis(1, 1) = accel_lin(1, 1);
yAxis = xAxis;
zAxis = xAxis;

for i = 2:n
    elapsed = (accel_lin(i, 1) - accel_lin(i - 1, 1)) / 1e9;
    xAxis(i, 1) = accel_lin(i, 1);
    xAxis(i, 2) = xAxis(i - 1, 2) + accel_lin(i - 1, 2) * elapsed;
    xAxis(i, 3) = xAxis(i - 1, 3) + xAxis(i - 1, 2) * elapsed + accel_lin(i - 1, 2) * elapsed * elapsed / 2;
   
    yAxis(i, 1) = accel_lin(i, 1);
    yAxis(i, 2) = yAxis(i - 1, 2) + accel_lin(i - 1, 3) * elapsed;
    yAxis(i, 3) = yAxis(i - 1, 3) + yAxis(i - 1, 2) * elapsed + accel_lin(i - 1, 3) * elapsed * elapsed / 2;
       
    zAxis(i, 1) = accel_lin(i, 1);
    zAxis(i, 2) = zAxis(i - 1, 2) + accel_lin(i - 1, 4) * elapsed;
    zAxis(i, 3) = zAxis(i - 1, 3) + zAxis(i - 1, 2) * elapsed + accel_lin(i - 1, 4) * elapsed * elapsed / 2;
end

%% Plot speed
figure();
hold on;
plot(xAxis(:, 2), 'b');
plot(yAxis(:, 2), 'r');
plot(zAxis(:, 2), 'k');
legend('vX','vY','vZ');
hold off;

%% Plot distance travelled
figure();
hold on;
plot(xAxis(:, 3), 'b');
plot(yAxis(:, 3), 'r');
plot(zAxis(:, 3), 'k');
legend('sX','sY','sZ');
hold off;

%% plot accel variance in window
win = 10;
varX = [];
varY = [];
varZ = [];
for i=1:win:(size(accel_lin,1)-win)
    varX = [varX var(accel(i:(i+win),2))];
    varY = [varY var(accel(i:(i+win),3))];
    varZ = [varZ var(accel(i:(i+win),4))];
end
figure();
hold on;
plot(varX, 'b');
plot(varY, 'r');
plot(varZ, 'g');
legend('varX','varY','varZ');
hold off;

%% plot accel mean in window
win = 10;
meanX = [];
meanY = [];
meanZ = [];
for i=1:win:(size(accel_lin,1)-win)
    meanX = [meanX mean(accel(i:(i+win),2))];
    meanY = [meanY mean(accel(i:(i+win),3))];
    meanZ = [meanZ mean(accel(i:(i+win),4))];
end
figure();
hold on;
plot(meanX, 'b');
plot(meanY, 'r');
plot(meanZ, 'g');
legend('meanX','meanY','meanZ');
hold off;

%% mean of accel
mean(accel_lin(:,2))
mean(accel_lin(:,3))
mean(accel_lin(:,4))